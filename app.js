const Discord = require('discord.js');
const client = new Discord.Client();
const moment = require('moment');
const settings = require('./settings/settings.json');
moment().locale("fr");

client.on('ready', () => {
    console.log(`Connecté en tant que ${client.user.tag}!\nLien d'invitation : https://discordapp.com/oauth2/authorize?client_id=468767743198363659&scope=bot&permissions=0`);
    client.user.setActivity('Soleria', { type: 'WATCHING' })
});

client.on('message', async message => {
    if(message.author.bot) return;
    if(message.content.indexOf(settings.prefix) !== 0) return;
    const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if(command === "ping") {
        const m = await message.channel.send("Ping?");
        m.edit(`Pong! La latence avec le bot est de ${m.createdTimestamp - message.createdTimestamp}ms. La latence avec l'API est de ${Math.round(client.ping)}ms`);
    }

    if(command === "kick") {
        if(!message.member.hasPermission("KICK_MEMBERS"))
          return message.reply("Vous n'avez pas la permission d'utiliser cette commande ! - (ERR : NO PERMS)");
        
        let member = message.mentions.members.first() || message.guild.members.get(args[0]);
        if(!member)
          return message.reply("Merci de mentionner un utilisateur - (ERR : NO MENTION)");
        if(!member.kickable) 
          return message.reply("Je ne peux pas kick cet utilisateur ! A-t-il un grade supérieur au mien ? - (ERR : BAD BOT PERMS)");
        
        let reason = args.slice(1).join(' ');
        if(!reason) reason = "Pas de raison";
        
        await member.kick(reason)
          .catch(error => message.reply(`Désolé ${message.author}, j'obtiens l'erreur suivante : ${error}`));
        message.reply(`${member.user.tag} à été kick par ${message.author.tag} car: ${reason}`);
    
    }
})

client.login(settings.token)